# Hashcat

**hashcat_pack:** ein Tool zum Analysieren von Passwortlisten und Generieren von Hashcat Regeln. Quelle: https://iphelix.medium.com/tool-release-password-analysis-and-cracking-kit-31a3587f550f


**OneRuletoRuleThemAll:** Sehr gute Regel zum Erzeugen von neuen Passwörtern
https://notsosecure.com/one-rule-to-rule-them-all


## Hashcat: Passwortlisten mit Regelbasierten Angriff erzeugen.

> Gutes Tutorial: https://infinitelogins.com/2020/11/16/using-hashcat-rules-to-create-custom-wordlists/

Befehl:
````bash
hashcat --force <passwortliste> -r <regel> -l 1000 --stdout > generated_passwords.txt
````

Über den Parameter `-l` kann die maximale Ausgabe limitiert werden.