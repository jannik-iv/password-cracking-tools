# Markov-Modelle
## **OMEN:** 
Ein in C geschriebener Passwort Generator, der das Prinzip der Markov Modelle anwendet. 
Quellen: 
- Artikel: https://hal.archives-ouvertes.fr/hal-01112124/document
- Code: https://github.com/RUB-SysSec/OMEN

#### Installation
Falls die Programme ``createNG`` und ``enumNG`` nicht im Verzeichnis auftauchen, müssen diese selbst kompiliert werden:

````batch
sudo apt-get install build-essential git
cd OMEN
make
````

#### Training
````batch
$ ./createNG --iPwdList password-training-list.txt
````

#### Sampling
````batch
$ ./enumNG -p -m 10000
````

----

## **markov-passwords-go:** 
Ein in Go geschriebener Passwort Generator. 
Die Implementierung wurde angepasst.
Quellen:
- https://github.com/bujimuji/markov-passwords

#### Training
````bash
./bin/mkpass -infile assets/train.txt -train trained.json
````



#### Sampling
````bash
./bin/mkpass -sample trained.json -m 1000000 > generated_pass_10e6.txt
````
----



## **markov-passwords-master:**
Ein in Python geschriebener Passwort Generator.
Quellen: 
- https://github.com/brannondorsey/markov-passwords

#### Training
````bash
python3 train.py
````
#### Sampling
````bash
python3 sample.py -n 1000000 -o generated_pass_10e6.txt
````