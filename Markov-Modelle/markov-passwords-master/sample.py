import pickle
import time
import argparse
import numpy as np


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--number-generate', '-n',
	                type=int,        
			default='10000',
                        dest='number_generate',
                        )

    parser.add_argument('--output-file', '-o',
                        default='samples.txt',
                        dest='output_file',
                        )
    
    return parser.parse_args()

args = parse_args()

max_ngrams = 3 # ngram size
num_generate = args.number_generate # number of passwords to generate

rng = np.random.default_rng()

def save(pw):
    with open(args.output_file, 'a',encoding='utf-8') as f:
        f.write(pw + "\n")


# generate a single new password using a stats dict
# created during the training phase 
def gen_password(stats, n):
	output = '`' * n
	for i in range(100):
		output += gen_char(output[i:i + n])
		if output[-1] == '\n':
			return output[0:-1].replace('`', '')[0:-1]

# Sample a character if the ngram appears in the stats dict.
# Otherwise recursively decrement n to try smaller grams in
# hopes to find a match (e.g. "off" becomes "of").
# This is a deviation from a vanilla markov text generator
# which one n-size. This generator uses all values <= n.
# preferencing higher values of n first. 
def gen_char(ngram):
	#print(stats[ngram].keys())
	if ngram in stats:
		# sample from the probability distribution
		return rng.choice(list(stats[ngram].keys()), p=list(stats[ngram].values()))
	else:
		print('{} not in stats dict'.format(ngram))
		return gen_char(ngram[0:-1])

with open('data/{}-gram.pickle'.format(max_ngrams),'rb') as file:
	stats = pickle.load(file)


# start = time.time()



for i in range(num_generate):
    if i % 10000 == 0:{
        print(i)
    }	    
    pw = gen_password(stats, max_ngrams)
    if pw is not None:
	    save(pw)
    

# print('finished in {:.2f} seconds'.format(time.time() - start))

