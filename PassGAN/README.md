# PassGAN


Angepasste Version von [@brannondorsey/PassGAN](https://github.com/brannondorsey/PassGAN) bzw. [@d4ichi/PassGAN](https://github.com/d4ichi/PassGAN) für Python 3 & TensorFlow 1.13.

## Änderungen:
Aufgrund des Fehlers *"Could not create cudnn handle: CUDNN_STATUS_INTERNAL_ERROR"*, der in Zusammenhang mit neuen GPUs auftritt, musste eine kleine Anpassung vorgenommen werden. siehe: https://github.com/tensorflow/tensorflow/issues/24496
Die Änderungen wurden in der train.py sowie in der sample.py durchgeführt.

````python
# Fix for cuDNN Error
config = tf.ConfigProto()
config.gpu_options.allow_growth= True
...
tf.Session(config=config)
````

## Installation

> Funktionierende Software Versionen:
> - Nvidia Treiber 495.29.05
> - CUDA Version 10.0.130
> - CUDNN Version 7.4.2
> - Python 3.7.3

```bash
pip3 install -r requirements.txt
```

### Training 
1. Passwortliste beschaffen:
   - https://hashmob.net/
   - [SecLists/Passwords](https://github.com/danielmiessler/SecLists/tree/master/Passwords) 

2. Trainingssatz und Testsatz aufteilen mit [/bin/prep-data.py](/PassGAN/bin/prep-data.py)
	>**Wichtig:** Im Programm den Pfad zur Passwortliste festlegen

3. Training starten
	````python
	python3 train.py --output-dir output --training-data data/train.txt
	````
	 **Parameter:**

	  `--training-data, -i`:  Eingabedatei fürs Training

	  `--output-dir, -o`: 	Ordner in dem das trainierte Modell gespeichert wird

	  `--save-every, -s`: Nach wievielen Iterationen ein Modell gespeichert wird 
	  (Standard: 5000)

	  `--iters, -n`: Anzahl an Trainingsiterationen (Standard: 200000)

	  `--batch-size, -b`: Anzahl an Passwörtern die gleichzeitig gelernt werden (Standard: 64)

	  `--seq-length, -l`: Maximale Passwortlänge (Standard: 10)

	  `--layer-dim, -d`: Dimension des Generator und Diskriminator Netzwerks (Standard: 128)

	  `--critic-iters, -c`: Anzahl an Diskriminator Trainingseinheiten pro Generator Training (Standard: 10)

	  `--lambda, -p`: Faktor der auf die Verlustfunktion des Diskrimnators gerechnet wird (Standard: 10)


### Sampling

````python
python3 sample.py \
  --input-dir output \
  --checkpoint output/checkpoints/checkpoint_200000.ckpt \
  --output generated_pass_10e6.txt
  --num-samples 1000000
````

Multi-GPU Sampling:
Über die Variable CUDA_VISIBLE_DEVICES die GPU festlegen auf der das Sampling stattfindet.

````bash
# Sampling auf GPU=1
CUDA_VISIBLE_DEVICES=1 python3 PassGAN/sample.py \
  --input-dir PassGAN/output \
  --checkpoint PassGAN/output/checkpoints/checkpoint_200000.ckpt \
  --output generated_pass_10e6.txt
  --num-samples 1000000 &

# Sampling auf GPU=2
CUDA_VISIBLE_DEVICES=2 python3 PassGAN/sample.py \
  --input-dir PassGAN/output \
  --checkpoint PassGAN/output/checkpoints/checkpoint_200000.ckpt \
  --output generated_pass_10e6.txt
  --num-samples 1000000 &
````

>**Wichtig:** Die Parameter `batch-size`, `seq-length` und `layer-dim` sollten mit denen des Trainings übereinstimmen!

 **Parameter:**

  `--input-dir, -i`: Ordner in dem das Trainierte Modell liegt

  `--checkpoint, -c`: Checkpoint der fürs Sampling genutz werden soll

  `--output, -o`: Speicherort für die erzeugten Passwörter

  `--num-samples, -n`: Anzahl an Passwörter die generiert werden sollen

  `--batch-size, -b`: Anzahl an Passwörtern die gleichzeitig gelernt wurden (Standard: 64)

  `--seq-length, -l`: Maximale Passwortlänge (Standard: 10)

  `--layer-dim, -d`: Dimension des Generator und Diskriminator Netzwerks (Standard: 128)
  