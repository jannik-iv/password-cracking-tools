# Password Cracking Tools

Codebasis mit der die Test in der Bachelorarbeit *Passwort Cracking - Vergleich moderner Machine Learning Ansätze und klassischer Methoden* durchgeführt wurden

## Aufbau des Repositorys
- [Hashcat](/Hashcat)
  
  OneRuleToRuleThemAll Regelsatz und das Analyse Tool [PACK](https://iphelix.medium.com/tool-release-password-analysis-and-cracking-kit-31a3587f550f). Zusätzlich die Information wie man mit Hashcat Passwortlisten erstellt.

- [Markov-Modelle](/Markov-Modelle)

  Verschiedene Implemeniterungen von Markov Modellen um Passwörter zu generieren.

- [PassGAN](/PassGAN)
  Machine Learning Programm um Passwöter zu lernen und zu generieren

- [Tools](/Tools)
  Hilfsprogramme für die Auswertung



## Kurzübersicht: Testbefehle
### Passwörter Generieren
**PassGAN**
Training:
````bash
# Training mit den Standard Hyperparametern
python3 PassGAN/train.py --output-dir output --training-data PassGAN/data/train.txt
````

Sampling:
````bash
# Sampling von 10^6 Passwörtern
python3 PassGAN/sample.py \
  --input-dir PassGAN/output \
  --checkpoint PassGAN/output/checkpoints/checkpoint_200000.ckpt \
  --output generated_pass_10e6.txt
  --num-samples 1000000
````

Multi-GPU Sampling:
Über die Variable CUDA_VISIBLE_DEVICES die GPU festlegen auf der das Sampling stattfindet.

````bash
# Sampling auf GPU=1
CUDA_VISIBLE_DEVICES=1 python3 PassGAN/sample.py \
  --input-dir PassGAN/output \
  --checkpoint PassGAN/output/checkpoints/checkpoint_200000.ckpt \
  --output generated_pass_10e6.txt
  --num-samples 1000000 &

# Sampling auf GPU=2
CUDA_VISIBLE_DEVICES=2 python3 PassGAN/sample.py \
  --input-dir PassGAN/output \
  --checkpoint PassGAN/output/checkpoints/checkpoint_200000.ckpt \
  --output generated_pass_10e6.txt
  --num-samples 1000000 &
````

**Markov-Modelle**
Training:
````bash
# Python:
cd Markov-Modelle/markov-passwords-master
python3 train.py

# Go:
cd Markov-Modelle/markov-passwords-go
./bin/mkpass -infile assets/train.txt -train trained.json

# OMEN:
cd Markov-Modelle/OMEN
./createNG --iPwdList train.txt
````

Sampling:
````bash
# Python:
cd Markov-Modelle/markov-passwords-master
python3 sample.py -n 1000000 -o generated_pass_10e6.txt

# Go:
cd Markov-Modelle/markov-passwords-go
./bin/mkpass -sample trained.json -m 1000000 > generated_pass_10e6.txt

# OMEN:
cd Markov-Modelle/OMEN
./enumNG -p -m 1000000 > generated_pass_10e6.txt
````

**Hashcat**
````bash
hashcat --force train.txt -r /usr/share/hashcat/rules/best64.rule -l 1400 --stdout > generated_pass_10e6.txt
````
### Auswertung
````bash
#einzigartige Passwörter identifizieren
Linux Befehl: grep -Fxvf <trainings-datei> <passwortliste> | sort -u

#Treffer im Testsatz finden
Linux Befehl: grep -Fxf <test-datei> <passwortliste> | sort -u
````
oder 
````python
cd Tools
python3 unique.py -r <trainings-datei> -d <passwortliste>
python3 intersection.py -r <passwortliste> -d <test-datei>
````

### Repetition Rate
Einfacher Weg um die Anzahl einzigartiger Passwörter zu zählen
````bash
Linux Befehl: cat <passwortliste> | sort -u | wc
````

### False Negatives
Abfrage der Passwörter gegen die haveibeenpwnd.com Datenbank
````bash
cd Tools
pyhton3 hibp.py -i <passwortliste> -o hibp_found.txt
````

### Kombination
Einfacher Weg um Passwortlisten miteine
````bash
cat <passwortliste1> <passwortliste2> | sort -u > combined_passwordlist.txt
````

## Links:
Weitere Informationen findet man unter folgenden Links:
  - Hashcat
    - [hashcat: Regelbasierter Angriff](https://hashcat.net/wiki/doku.php?id=rule_based_attack)
  - Markov-Modelle:
    - [Python](https://github.com/brannondorsey/markov-passwords)
    - [Go](https://github.com/bujimuji/markov-passwords)
    - [OMEN](https://github.com/RUB-SysSec/OMEN)
- PassGAN
  - [Offizieller Code](https://github.com/brannondorsey/PassGAN)
  - [Python3 Portierung](https://github.com/d4ichi/PassGAN)
