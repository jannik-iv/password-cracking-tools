#!/bin/bash
test=./rockyou/test.txt
train=./rockyou/train.txt
proc=0
size=500M
evaluate(){
	echo "Unique-Check: "$1
	grep -Fxvf $train $1 | sort -u > unique_"${file%%.*}".txt
	
	echo "Intersection-Check: "unique_$1.txt
	grep -Fxf unique_$1.txt $test | sort -u > inters_"${file%%.*}".txt
	
	rm $1
}

if [ -z "$1" ]
then
	echo "Error: filename missig">>/dev/stderr
else
	base_name=$(basename $1)
	filename="${base_name%%.*}"
	prefix="$filename"_
	
	echo "Splitting "$1
	split -b $size $1 $prefix
	
	for file in $prefix*
	do
		if [[ $proc -gt 4 ]]
		then	
			wait -f $(jobs -p)
			proc=0
			sleep 5
		fi
		evaluate $file &
		proc=$((proc+1))
	done
	wait -f $(jobs -p)
		
	echo "Speichere Ergebnisse"
	#cat unique_$prefix* > unique_$base_name
	#cat inters_$prefix* > inters_$base_name
	
	#rm unique_$prefix*
	#rm inters_$prefix*
	rm $prefix*
fi

