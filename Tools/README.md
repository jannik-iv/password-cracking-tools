# Hilfsprogramme

**`eval.sh:`** Programm um große Passwortlisten auszuwerten.
Es teilt ein Datei in kleinere Dateien auf und ermittelt anschließend
die einzigartigen und die im Testsatz gefundenen Passwörter.
> Wichtig: Die **test** und **train** Variablen müssen auf den Trainings- und Testsatz zeigen

Ausführung:
Bei der Ausführung muss der Pfad zur Passwortliste übergeben werden
````bash
./eval.sh password.txt
````
----

**`hibp.py:`** Programm um eine Passwortliste mit der haveibeenpwnd.com Datenbank abzugleichen und die Treffer zu ermitteln.
> Parameter:
> - --input-file, -i : Passwortliste die gegen haveibeenpwnd.com geprüft wird
> - --output-file, -o : Datei, in der die Treffer gespeichert werden

Ausführung:
````python
python3 hibp.py -i generated_passwords.txt -o hibp.txt
````

----
**`intersection.py:`** Programm um Überschneidungen zweier Passwortlisten zu ermitteln
> Parameter:
> - --reference, -r : 1. Passwortliste
> - --difference, -d : 2. Passwortliste
> - --output-file, -s : Datei, in der die Treffer gespeichert werden

----
**`unique.py:`** Programm um einzigartie Passwörter aus einer Passwortliste zu ermitteln
> Parameter:
> - --reference, -r : Passwortliste, in der die einzigartigen Passwörter ermittelt werden sollen
> - --difference, -d : Passwortliste zum Vergleich
> - --output-file, -s : Datei, in der die einzigartigen Passwörter gespeichert werden
