import argparse

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--reference', '-r',
                        required=True,
                        dest='reference',
                        )

    parser.add_argument('--difference', '-d',
                        required=True,
                        dest='difference',
                        )

    parser.add_argument('--output-file', '-s',
                        default='unique.txt',
                        dest='output_file',
                        )

    
    
    return parser.parse_args()

args = parse_args()

reference = set(open(args.reference))
difference = set(open(args.difference))

diff = reference.intersection(difference)

(open(args.output_file, 'w')).write("".join(str(item) for item in diff))
