import hashlib
import requests
import ast
import argparse

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--input-file', '-i',
                        required=True,
                        dest='input_file',
                        )

    parser.add_argument('--output-file', '-o',
                        default='hibp_found.txt',
                        dest='output_file',
                        )

    
    
    return parser.parse_args()

args = parse_args()


with open(args.input_file, encoding='utf-8') as file:
    pwList = (list(file))

conv_pwList = list()
for item in pwList:
    conv_pwList.append(item.strip())


f = open(args.output_file, 'a')
pwfound = []
counter = 0
for pw in conv_pwList:
    #counter += 1
    #if counter%100 == 0:
    #    print(counter)

    hash = (hashlib.sha1(pw.encode('utf-8')).hexdigest())
    url = 'https://api.pwnedpasswords.com/range/'+hash[0:5]
    r = requests.get(url)
    text = r.text
    if hash[5:] in text.lower():
        f.write(pw + '\n')
